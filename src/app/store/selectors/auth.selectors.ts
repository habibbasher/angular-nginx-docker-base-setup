import { createFeatureSelector, createSelector } from "@ngrx/store";

import { AuthState, StateE } from "../states";

export const selectAuthState = createFeatureSelector<AuthState>(StateE.Auth);

export const isLoggedIn = createSelector(selectAuthState, auth => !!auth.user);

export const isLoggedOut = createSelector(isLoggedIn, loggedIn => !loggedIn);
