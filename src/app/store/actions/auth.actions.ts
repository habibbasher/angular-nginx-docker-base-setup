import { createAction, props } from '@ngrx/store';

import { UserI } from '../../auth/models/user.interface';
import { EAuthActions } from './action.enums';

export const login = createAction(
  EAuthActions.UserLogin,
  props<{ user: UserI }>()
);

export const autoSignin = createAction(
  EAuthActions.AutoSignin,
  props<{ user: UserI }>()
);

export const logout = createAction(EAuthActions.UserLogout);
