export enum EAuthActions {
  UserLogin = '[Login Page] User Login',
  UserLogout = '[Top Menu] Logout',
  AutoSignin = 'AutoSignin',
}