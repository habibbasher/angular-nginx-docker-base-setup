import { UserI } from "../../auth/models/user.interface";

interface AuthState {
  user: UserI;
}

export default AuthState;
