import { createReducer, on } from "@ngrx/store";

import { AuthActions } from "../actions";
import { AuthState } from "../states";

export const initialAuthState: AuthState = {
  user: undefined
};

export const authReducer = createReducer(
  initialAuthState,

  on(AuthActions.login, (state, action) => {
    console.log('login action: ', action);
    return {
      user: action.user
    };
  }),

  on(AuthActions.autoSignin, (state, action) => {
    console.log('auto signin action: ', action);
    return {
      user: action.user
    };
  }),

  on(AuthActions.logout, (state, action) => {
    return {
      user: undefined
    };
  })
);
