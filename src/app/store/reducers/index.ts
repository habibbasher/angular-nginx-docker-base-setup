import { reducers, metaReducers } from './app.reducers';
import { authReducer } from './auth.reducers';

export {
  reducers,
  metaReducers,
  authReducer
};