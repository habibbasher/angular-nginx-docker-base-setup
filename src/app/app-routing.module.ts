import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home/home.component";

// import { AuthGuard } from "./shared/services/auth.guard";

const routes: Routes = [
  {
    path: "signup",
    redirectTo: "/signup"
  },
  {
    path: "signin",
    redirectTo: "/signin"
  },
  {
    path: "dashboard",
    loadChildren: () =>
      import("./dashboard/dashboard.module").then(m => m.DashboardModule),
    // without rbac simple authguard will work fine
    // canActivate: [AuthGuard]
    canActivate: ["adminsOnlyGuard"]
  },
  // {
  //   path: "home",
  //   component: HomeComponent,
  //   canActivate: ["usersOnlyGuard"]
  // },
  {
    path: "**",
    redirectTo: "/signin"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
