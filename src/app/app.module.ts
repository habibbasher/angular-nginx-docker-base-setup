import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { Router } from "@angular/router";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ServiceWorkerModule } from "@angular/service-worker";

import { CookieService } from "ngx-cookie-service";
import { StoreModule } from "@ngrx/store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { EffectsModule } from "@ngrx/effects";
import { RouterState, StoreRouterConnectingModule } from "@ngrx/router-store";

// Material
import { MatSidenavModule } from "@angular/material/sidenav";

// @ng-bootstrap

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { environment } from "../environments/environment";

import { reducers, metaReducers } from "./store/reducers";

import { AuthModule } from "./auth/auth.module";
import { LayoutModule } from "./layout/layout.module";
import { SharedModule } from "./shared/shared.module";

import { AuthInterceptorService } from "./shared/services/auth-interceptor.service";
import { HomeComponent } from "./home/home.component";
import { ContactComponent } from "./contact/contact.component";
import { AuthService } from "./auth/services/auth.service";
import { AuthGuard } from "./shared/services/auth.guard";
import { AlertModalComponent } from './shared/components/alert-modal/alert-modal.component';

export function createAdminOnlyGuard(authService: AuthService, router: Router) {
  return new AuthGuard(["admin"], authService, router);
}

// export function createUserOnlyGuard(authService: AuthService, router: Router) {
//   return new AuthGuard(["user"], authService, router);
// }

@NgModule({
  declarations: [AppComponent, HomeComponent, ContactComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    AppRoutingModule,
    SharedModule,
    LayoutModule,
    AuthModule.forRoot(),
    ServiceWorkerModule.register("ngsw-worker.js", {
      enabled: environment.production
    }),
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
        strictActionSerializability: true,
        strictStateSerializability: true
      }
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production
    }),
    EffectsModule.forRoot([]),
    StoreRouterConnectingModule.forRoot({
      stateKey: "router",
      routerState: RouterState.Minimal
    })
  ],
  providers: [
    CookieService,
    {
      provide: "adminsOnlyGuard",
      useFactory: createAdminOnlyGuard,
      deps: [AuthService, Router]
    },
    // {
    //   provide: "usersOnlyGuard",
    //   useFactory: createUserOnlyGuard,
    //   deps: [AuthService, Router]
    // },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    AlertModalComponent
  ]
})
export class AppModule { }
