export interface UserI {
  id: string;
  email: string;
  token: string;
  expiresIn: number;
  roles?: string;
  username?: string;
}
