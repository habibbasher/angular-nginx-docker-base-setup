import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { SignupComponent } from "./signup/signup.component";
import { SigninComponent } from "./signin/signin.component";

const routes: Routes = [
  {
    path: "signin",
    component: SigninComponent,
    data: { showHeader: false, showSidebar: false, showFooter: false }
  },
  {
    path: "signup",
    component: SignupComponent,
    data: { showHeader: false, showSidebar: false, showFooter: false }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {}
