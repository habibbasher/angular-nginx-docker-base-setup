import { NgModule, ModuleWithProviders } from "@angular/core";

import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { authReducer } from "../store/reducers";
import { StateE } from "../store/states";

import { AuthEffects } from "../store/effects/auth.effects";

import { AuthRoutingModule } from "./auth-routing.module";

import { SigninComponent } from "./signin/signin.component";
import { SignupComponent } from "./signup/signup.component";
import { AuthService } from "./services/auth.service";

import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [SigninComponent, SignupComponent],
  imports: [
    SharedModule,
    AuthRoutingModule,
    StoreModule.forFeature(StateE.Auth, authReducer),
    EffectsModule.forFeature([AuthEffects])
  ]
})
export class AuthModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AuthModule,
      providers: [AuthService]
    };
  }
}
