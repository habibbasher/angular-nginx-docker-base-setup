import { Component, OnInit, ComponentFactoryResolver, ViewChild, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

import { Store } from "@ngrx/store";
import { noop, Subscription } from "rxjs";
import { tap } from "rxjs/operators";

import { AppState } from "../../store/states";
import { AuthActions } from "../../store/actions";
import { AuthService } from "../services/auth.service";
import { httpErrorParser } from "../../shared/utilities/error.parser";
import { AlertModalComponent } from 'src/app/shared/components/alert-modal/alert-modal.component';
import { DcplaceholderDirective } from 'src/app/shared/directives/dcplaceholder/dcplaceholder.directive';

@Component({
  selector: "app-signin",
  templateUrl: "./signin.component.html",
  styleUrls: ["./signin.component.scss"]
})
export class SigninComponent implements OnInit, OnDestroy {
  signinForm: FormGroup;
  fgpForm: FormGroup;

  // set static: true if you use in ngInit method.
  @ViewChild(DcplaceholderDirective, { static: false }) alertHost: DcplaceholderDirective;
  private closeSubscription$: Subscription;

  isLoading = false;
  isSubmitted = false;
  isLoginError = false;

  error = null;

  loginButtonText = "Login";
  loginWithGmailBtnText = "Login With Gmail";

  loginBtnClasses = "btn btn-primary btn-block rounded-btn";
  loginWithGmailBtnClasses = "btn btn-outline-primary btn-block rounded-btn"

  constructor(
    private fb: FormBuilder,
    private cfr: ComponentFactoryResolver,
    private router: Router,
    private store: Store<AppState>,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.initializeLoginForm();
    this.initializeForgotPasswordForm();

  }

  initializeLoginForm() {
    this.signinForm = this.fb.group({
      email: [null, Validators.required],
      password: [null, Validators.required]
    });
  }

  initializeForgotPasswordForm() {
    this.fgpForm = this.fb.group({
      email: [null, Validators.required]
    });
  }

  onHandleClick() {
    console.log('button click handler!!!');
  }

  onSubmitSigninForm() {

    const { email, password } = this.signinForm.value;

    this.authService
      .signIn({ email, password })
      .pipe(
        tap(res => {
          const { user, token, expiresIn } = res.data;
          this.store.dispatch(AuthActions.login({ user: { ...user, token, expiresIn } }));
          this.router.navigateByUrl("/dashboard");
        })
      )
      .subscribe(noop, error => {
        console.log('error: ', error);
        this.showErrorAlert("This is a test!!!");
        // this.error = httpErrorParser(error);
        // console.log(4444, httpErrorParser(error));
      });
  }

  onLoginWithGmail() { }

  onSubmitFPForm() { }

  private showErrorAlert(message: string) {
    const alertComponentFactory = this.cfr.resolveComponentFactory(AlertModalComponent);
    const hostViewContainerRef = this.alertHost.viewContainerRef;
    hostViewContainerRef.clear();

    const componentRef = hostViewContainerRef.createComponent(alertComponentFactory);
    componentRef.instance.message = message;
    this.closeSubscription$ = componentRef.instance.close.subscribe(() => {
      this.closeSubscription$.unsubscribe();
      hostViewContainerRef.clear();
    });
  }

  ngOnDestroy() {
    if (this.closeSubscription$) {
      this.closeSubscription$.unsubscribe();
    }
  }
}
