import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { HttpErrorResponse } from "@angular/common/http";

import { Store } from "@ngrx/store";
import { BehaviorSubject, throwError, Subject } from "rxjs";
import { tap, catchError } from "rxjs/operators";

import { AppState } from "../../store/states";

import { UserI } from "../models/user.interface";
import { SignInI } from "../models/signin.interface";
import { SignUpI } from "../models/signup.interface";
import { ApiService } from "../../shared/services/api.service";
import { StorageService } from "../../shared/services/storage.service";
import { IdentifiersE } from "../../shared/utilities/application.identifiers";
import { httpErrorParser } from "../../shared/utilities/error.parser";
import { AuthActions } from "src/app/store/actions";

@Injectable({
  providedIn: "root"
})
export class AuthService {

  user$ = new BehaviorSubject<UserI>(null);

  // private tokenExpirationTimer: any;

  constructor(
    private apiService: ApiService,
    private storageService: StorageService,
    private router: Router,
    private store: Store<AppState>
  ) { }

  signIn(credentials: SignInI) {
    return this.apiService
      .callApi(IdentifiersE.POST, IdentifiersE.SIGNIN_PATH, null, credentials)
      .pipe(
        catchError(this.handleError),
        tap(res => {
          const { user, token, expiresIn } = res.data;
          this.handleAuthentication({ ...user, token, expiresIn: +expiresIn });
        })
      );
  }

  // signUp(credentials: SignUpI) {
  //   return this.apiService
  //     .callApi(IdentifiersE.POST, IdentifiersE.SIGNUP_PATH, null, credentials)
  //     .pipe(
  //       catchError(this.handleError),
  //       tap(response => {
  //         console.log("tap response", response);
  //         const { user, token, expiresIn } = response.data;
  //         this.handleAuthentication(
  //           user,
  //           token,
  //           +expiresIn
  //         );
  //       })
  //     );
  // }

  autoSignin() {
    const storedUser: UserI = this.storageService.getUser();
    if (!storedUser) {
      return;
    }
    if (storedUser.token) {
      this.user$.next(storedUser);
      this.store.dispatch(AuthActions.autoSignin({ user: storedUser }));
      // const expirationDuration =
      //   new Date(storedUser["_tokenExpiration"]).getTime() -
      //   new Date().getTime();
      // this.autoLogout(expirationDuration);
    }
  }

  // autoLogout(expiration: number) {
  //   this.tokenExpirationTimer = setTimeout(() => {
  //     this.signOut();
  //   }, expiration);
  // }

  // signOut() {
  //   this.user.next(null);
  //   this.router.navigate(["/auth"]);
  //   this.storageService.removeUser();
  //   if (this.tokenExpirationTimer) {
  //     clearTimeout(this.tokenExpirationTimer);
  //   }
  //   this.tokenExpirationTimer = null;
  //   // return this.apiRequestService
  //   //   .callApi('post', 'auth/logout', null, null)
  //   //   .pipe(catchError(this.handleError));
  // }

  private handleAuthentication(userInfo: UserI) {
    // const expiration = new Date(new Date().getTime() + expiresIn * 1000);
    this.user$.next(userInfo);
    // this.autoLogout(expiresIn * 1000);
    // this.storageService.setUser(user);
  }

  private handleError(error: HttpErrorResponse) {
    console.log("handleError method in auth service: error", error);
    let errorMessage = "An unknown error occurred!";
    if (!error.error || !error.error.message) {
      return throwError(errorMessage);
    }
    // TODO: We can check errors here
    // and give more specific error message to subscriber
    //  Need to do it later.

    // switch (error.error.message) {
    //   case 'Email exist':
    // errorMessage = 'An unknown error occurred!';
    //     break;

    //   default:
    //     break;
    // }
    errorMessage = error.error.message;
    return throwError(errorMessage);
  }
}
