import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

import { CookieService } from 'ngx-cookie-service';
import { AuthService } from './auth/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {

  title = 'angular-nginx-docker-base-setup';

  shouldShowHeader = false;
  shouldShowSidebar = false;
  shouldShowFooter = false;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cookieService: CookieService,
    private authService: AuthService
  ) { }

  ngOnInit() {

    this.authService.autoSignin();

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.shouldShowHeader =
          this.activatedRoute.firstChild.snapshot.data.showHeader !== false;
        this.shouldShowSidebar =
          this.activatedRoute.firstChild.snapshot.data.showSidebar !== false;
        this.shouldShowFooter =
          this.activatedRoute.firstChild.snapshot.data.showFooter !== false;
      }
    });
  }

  onActivate() {
    window.scroll(0, 0);
  }

  changeLanguage(lang: string) {
    this.cookieService.set('lang', lang);
    setTimeout(() => {
      location.reload();
    }, 1000);
  }

  ngOnDestroy() {
    this.authService.user$.subscribe();
  }
}
