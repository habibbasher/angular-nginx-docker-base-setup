import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

import { Store } from '@ngrx/store';

import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { AppState } from '../store/states';

import { ApiService } from '../shared/services/api.service';
import { StorageService } from '../shared/services/storage.service';
import { IdentifiersE } from "../shared/utilities/application.identifiers";

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(
    private apiService: ApiService,
    private storageService: StorageService,
    private router: Router,
    private store: Store<AppState>,
  ) { }

  getUsers() {
    return this.apiService
      .callApi(IdentifiersE.GET, IdentifiersE.GET_USERS_PATH)
      .pipe(
        catchError(this.handleError),
        tap(res => {
          console.log("getUsers response", res);
          // const { user, token, expiresIn } = response.data;
          // this.handleAuthentication({ ...user, token, expiresIn: +expiresIn });
        })
      );
  }

  private handleError(error: HttpErrorResponse) {
    console.log("handleError method in auth service: error", error);
    let errorMessage = "An unknown error occurred!";
    if (!error.error || !error.error.message) {
      return throwError(errorMessage);
    }
    // TODO: We can check errors here
    // and give more specific error message to subscriber
    //  Need to do it later.

    // switch (error.error.message) {
    //   case 'Email exist':
    // errorMessage = 'An unknown error occurred!';
    //     break;

    //   default:
    //     break;
    // }
    errorMessage = error.error.message;
    return throwError(errorMessage);
  }

}
