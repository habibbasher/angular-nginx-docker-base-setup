import { Component, OnInit } from '@angular/core';

import { noop } from 'rxjs';

import { DashboardService } from './dashboard.service';
import { httpErrorParser } from '../shared/utilities/error.parser';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  componentString = `dashboard works dashboard works dashboard works dashboard works!`;

  constructor(private dashboardService: DashboardService) { }

  ngOnInit() {
    this.dashboardService.getUsers().subscribe(noop, error => {
      // this.error = httpErrorParser(error);
      console.log('dashboardComponent', httpErrorParser(error));
    });
  }

}
