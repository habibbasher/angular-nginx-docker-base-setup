import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shorten'
})
export class ShortenPipe implements PipeTransform {

  transform(str: string, limit = 10): any {
    if (str.length > limit) {
      return `${str.substr(0, limit)}...`;
    }
    return str;
  }

}
