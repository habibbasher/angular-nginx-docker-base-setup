import { Injectable } from "@angular/core";

import { UserI } from "../../auth/models/user.interface";
import { IdentifiersE } from "../utilities/application.identifiers";

@Injectable({
  providedIn: "root"
})
export class StorageService {
  getUser(): UserI {
    return window.localStorage[IdentifiersE.CURRENT_USER]
      ? JSON.parse(window.localStorage[IdentifiersE.CURRENT_USER])
      : null;
  }

  setUser(user: UserI) {
    window.localStorage[IdentifiersE.CURRENT_USER] = JSON.stringify(user);
  }

  removeUser() {
    return localStorage.removeItem(IdentifiersE.CURRENT_USER);
  }

  getJwtToken(): string {
    return window.localStorage[IdentifiersE.JWT_TOKEN];
  }

  setJwtToken(token: string) {
    window.localStorage[IdentifiersE.JWT_TOKEN] = token;
  }

  removeJwtToken() {
    return localStorage.removeItem(IdentifiersE.JWT_TOKEN);
  }

  destroyAll() {
    window.localStorage.clear();
  }
}
