import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from "@angular/router";

import * as _ from "lodash";

import { Store, select } from "@ngrx/store";
import { Observable } from "rxjs";
import { tap, map, first } from "rxjs/operators";

import { AppState } from "../../store/states";
import { isLoggedIn } from "../../store/selectors/auth.selectors";
import { AuthService } from "src/app/auth/services/auth.service";
import { UserI } from "src/app/auth/models/user.interface";

@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate {
  constructor(
    private allowedRoles: string[],
    private authService: AuthService,
    private router: Router
  ) {}

  // constructor(
  //   private store: Store<AppState>,
  //   private router: Router,
  //   private authService: AuthService
  // ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.authService.user$.pipe(
      map(
        (user: UserI) => !!_.intersection(this.allowedRoles, user.roles).length
      ),
      first(),
      tap(allowed => {
        if (!allowed) {
          this.router.navigateByUrl("/login");
        }
      })
    );
  }

  // Without role based authorization following
  // implementation is also good one

  // canActivate(
  //   route: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot
  // ): Observable<boolean> {
  //   return this.store.pipe(
  //     select(isLoggedIn),
  //     tap((res: boolean) => {
  //       if (!res) {
  //         this.router.navigateByUrl("/login");
  //       }
  //     })
  //   );
  // }
}
