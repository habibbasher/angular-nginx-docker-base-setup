import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import {
  HttpInterceptor,
  HttpEvent,
  HttpHandler,
  HttpRequest,
  HttpHeaders,
  HttpErrorResponse
} from "@angular/common/http";

import { Observable, throwError, noop } from "rxjs";
import { take, exhaustMap, catchError, tap } from "rxjs/operators";

import { AuthService } from "../../auth/services/auth.service";
import { UserI } from "../../auth/models/user.interface";

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  // private user: UserI;

  constructor(private authService: AuthService, private router: Router) { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    // This is another way of intercepting

    // this.authService.user$
    //   .pipe(
    //     take(1),
    //     tap((res: UserI) => {
    //       this.user = res;
    //     })
    //   )
    //   .subscribe(noop);

    // if (
    //   !(req.url.includes("/login") || req.url.includes("/signup")) &&
    //   (!this.user || !this.user.token)
    // ) {
    //   this.router.navigateByUrl("/login");
    //   return throwError({ message: "Token is not provided" });
    // }

    // const modifiedReq = req.clone({
    //   headers: new HttpHeaders()
    //     .set("Content-Type", "application/json")
    //     .set("Access-Control-Allow-Origin", "*")
    //     .set("Authorization", `JWT ${this.user ? this.user.token : ""}`)
    // });

    // return next.handle(modifiedReq).pipe(
    //   catchError((err: HttpErrorResponse) => {
    //     if (err.status === 401) {
    //       this.router.navigateByUrl("/login");
    //     }
    //     return throwError(err);
    //   })
    // );

    return this.authService.user$.pipe(
      take(1),
      exhaustMap((user: UserI) => {

        if (
          !(req.url.includes("/login") || req.url.includes("/signup")) &&
          (!user || !user.token)
        ) {
          this.router.navigateByUrl("/login");
          return throwError({ message: "Token is not provided" });
        }

        if (!user) {
          return next.handle(req);
        }

        const modifiedReq = req.clone({
          headers: new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', `JWT ${user.token}`)
        });
        return next.handle(modifiedReq).pipe(
          catchError((err: HttpErrorResponse) => {
            if (err.status === 401) {
              this.router.navigateByUrl("/login");
            }
            return throwError(err);
          })
        );
      })
    );
  }
}
