import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";

import { IdentifiersE } from "../utilities/application.identifiers";
import { StorageService } from "./storage.service";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class ApiService {
  private baseURI = environment.baseApiURI;
  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) { }

  private setHeaders() {
    return new HttpHeaders({
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*"
    });
  }

  private setTokenWithHeaders() {
    return new HttpHeaders({
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*"
      // 'Authorization': this.localStorageService.getJwtToken()
    });
  }

  private get(path: string, params: HttpParams): Observable<any> {
    return this.http.get(`${this.baseURI}${path}`, { params });
  }

  private put(path: string, body: object): Observable<any> {
    return this.http.put(`${this.baseURI}${path}`, body);
  }

  private post(path: string, body: object): Observable<any> {
    return this.http.post(`${this.baseURI}${path}`, body);
  }

  private delete(path: string): Observable<any> {
    return this.http.delete(`${this.baseURI}${path}`);
  }

  callApi(
    requestType: string,
    path: string,
    params: HttpParams = null,
    body: object = null
  ) {
    switch (requestType.toLowerCase()) {
      case IdentifiersE.POST:
        return this.post(path, body);
      case IdentifiersE.PUT:
        return this.put(path, body);
      case IdentifiersE.GET:
        return this.get(path, params);
      case IdentifiersE.DELETE:
        break;
      case IdentifiersE.PATCH:
        break;
      default:
        break;
    }
  }
}
