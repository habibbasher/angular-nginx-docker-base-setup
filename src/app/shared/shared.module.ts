import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";

import { RbacDirective } from "./directives/rbac/rbac.directive";
import { DropdownDirective } from "./directives/dropdown/dropdown.directive";
import { DcplaceholderDirective } from './directives/dcplaceholder/dcplaceholder.directive';

import { ShortenPipe } from './pipes/shorten.pipe';
import { FilterPipe } from './pipes/filter.pipe';

import { LoadingSpinnerComponent } from "./components/loading-spinner/loading-spinner.component";
import { AlertModalComponent } from "./components/alert-modal/alert-modal.component";
import { ButtonComponent } from "./components/button/button.component";

@NgModule({
  declarations: [
    DropdownDirective,
    RbacDirective,
    DcplaceholderDirective,
    ShortenPipe,
    FilterPipe,
    LoadingSpinnerComponent,
    AlertModalComponent,
    ButtonComponent,
  ],
  imports: [CommonModule, HttpClientModule, ReactiveFormsModule],
  exports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    RbacDirective,
    DropdownDirective,
    DcplaceholderDirective,
    ShortenPipe,
    FilterPipe,
    LoadingSpinnerComponent,
    AlertModalComponent,
    ButtonComponent
  ]
})
export class SharedModule { }
