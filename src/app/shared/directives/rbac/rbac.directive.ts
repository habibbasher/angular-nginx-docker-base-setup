import {
  Directive,
  Input,
  TemplateRef,
  ViewContainerRef,
  OnDestroy
} from "@angular/core";

import { Subscription } from "rxjs";
import * as _ from "lodash";

import { AuthService } from "../../../auth/services/auth.service";
import { UserI } from "../../../auth/models/user.interface";

@Directive({
  selector: "[appRbac]"
})
export class RbacDirective implements OnDestroy {
  allowedRoles: string[];
  user: UserI;
  userSubscription: Subscription;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private authService: AuthService
  ) {
    this.userSubscription = authService.user$.subscribe((user: UserI) => {
      this.user = user;
      this.showIfUserAllowed();
    });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  @Input()
  set appRbac(allowedRoles: string[]) {
    this.allowedRoles = allowedRoles;
    this.showIfUserAllowed();
  }

  showIfUserAllowed() {
    if (!this.allowedRoles || !this.allowedRoles.length || !this.user) {
      this.viewContainer.clear();
      return;
    }

    const isUserAllowed = !!_.intersection(this.allowedRoles, this.user.roles)
      .length;

    if (isUserAllowed) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }
}
