import { Directive, HostListener, HostBinding } from "@angular/core";

@Directive({
  selector: "[appAuthorization]"
})
export class AuthorizationDirective {
  @HostBinding("class.show") isOpen = false;

  //   need to refactore this directive.It is created just for test.

  constructor() {}

  @HostListener("click") toggleOpen() {
    this.isOpen = !this.isOpen;
  }
}
