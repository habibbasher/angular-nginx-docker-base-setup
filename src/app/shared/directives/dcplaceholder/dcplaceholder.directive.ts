import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appDcplaceholder]'
})
export class DcplaceholderDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
