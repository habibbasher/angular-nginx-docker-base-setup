import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Input() isLoading: boolean;
  @Input() buttonText: string;
  @Input() classes: string;
  @Input() isSubmitButton: boolean;

  @Output() clickEvent = new EventEmitter<void>();

  constructor() {

  }

  ngOnInit() {
  }

  onClickHandler() {
    this.clickEvent.emit();
  }

}
