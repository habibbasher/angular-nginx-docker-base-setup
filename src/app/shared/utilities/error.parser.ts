import { HttpErrorResponse } from '@angular/common/http';
import * as _ from 'lodash';

const errorTraverser = (error: any) => {
  if (_.has(error, 'error')) {
    errorTraverser(error.error);
  } else {
    return error;
  }
};

export const httpErrorParser = (error: HttpErrorResponse) => {
  if (!_.isEmpty(error)) {
    let returnValue = error;
    if (_.has(error, 'error')) {
      httpErrorParser(error.error);
    } else {
      return returnValue;
    }
    return returnValue;
  }
};
