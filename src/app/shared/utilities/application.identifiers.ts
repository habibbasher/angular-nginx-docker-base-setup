export enum IdentifiersE {
  // Token related
  JWT_TOKEN = "jwt_token",
  REFRESH_TOKEN = "refresh_token",
  CURRENT_USER = "current_user",

  // Api pats
  SIGNIN_PATH = "users/login",
  SIGNUP_PATH = "users",
  GET_USERS_PATH = "users",

  // api call related
  POST = "post",
  PUT = "put",
  GET = "get",
  DELETE = "delete",
  PATCH = "patch"
}
