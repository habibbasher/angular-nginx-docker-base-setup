import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Store } from '@ngrx/store';

import { AppState } from '../../store/states';
import { AuthActions } from '../../store/actions';

import { AuthService } from '../../auth/services/auth.service';
import { StorageService } from '../../shared/services/storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    private router: Router,
    private storageService: StorageService,
    private authService: AuthService,
    private store: Store<AppState>,
  ) { }

  ngOnInit() {
  }

  onLogout() {
    this.store.dispatch(AuthActions.logout());
    this.storageService.destroyAll();
    this.authService.user$.next(null);
    this.router.navigateByUrl('/login');
  }

}
