import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";

import { FooterComponent } from "./footer/footer.component";
import { HeaderComponent } from "./header/header.component";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [FooterComponent, HeaderComponent, SidebarComponent],
  imports: [SharedModule, RouterModule],
  exports: [FooterComponent, HeaderComponent, SidebarComponent]
})
export class LayoutModule { }
